;;; Copyright © 2022 David Elsing <david.elsing@kit.edu>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with This program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (chemistry-channel rdcs)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix git)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system python)
  #:use-module (gnu packages)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-xyz))

(define-public python-rdc-tools
  (package
	(name "python-rdc-tools")
	(version "0.1")
	(source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://codeberg.org/dtelsing/RDC_tools")
			 (commit version)))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "1a3j0hqbjfzqhj2adcapxl6g9k6d3d3ahkcin2d6r6qbxyrxksw2"))))
	(build-system python-build-system)
	(arguments
	 '(#:phases
	   (modify-phases %standard-phases
		 (replace 'check
		   (lambda* (#:key tests? inputs outputs #:allow-other-keys)
			 (when tests?
			   (add-installed-pythonpath inputs outputs)
			   (invoke "pytest" "tests"))
			 #t)))))
	(propagated-inputs
	 (list python-numpy python-scipy))
	(native-inputs
	 (list python-pytest))
	(home-page "")
	(synopsis "Tools for RDCs")
	(description "Tools for RDCs")
	(license license:gpl3+)))
