;;; Copyright © 2022 David Elsing <david.elsing@kit.edu>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with This program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (chemistry-channel chemistry)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system perl)
  #:use-module (guix build-system python)
  #:use-module (gnu packages)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages check)
  #:use-module (gnu packages chemistry)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages machine-learning)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages video)
  #:use-module (gnu packages xorg))

(define-public python-parmed
  (package
	(name "python-parmed")
	(version "3.4.3")
	(source
	 (origin
	   (method url-fetch)
	   (uri (pypi-uri "ParmEd" version))
	   (sha256
		(base32 "0z32qrwsdv3wv45ry1ah8ini4r44jql2p4h2l0q95rpzwdav3bwh"))
	   (modules '((guix build utils)
				  (ice-9 ftw)
				  (srfi srfi-26)))
	   (snippet
		'(begin
		   (delete-file "versioneer.py")
		   (with-directory-excursion "parmed"
			 (for-each
			  delete-file-recursively
			  '("__pycache__"
				"utils/fortranformat/__pycache__"
				"utils/__pycache__"
				"unit/__pycache__"
				"tools/simulations/__pycache__"
				"tools/__pycache__"
				"tinker/__pycache__"
				"rosetta/__pycache__"
				"rdkit/__pycache__"
				"openmm/__pycache__"
				"namd/__pycache__"
				"modeller/__pycache__"
				"gromacs/__pycache__"
				"formats/pdbx/__pycache__"
				"formats/__pycache__"
				"charmm/__pycache__"
				"amber/mdin/__pycache__"
				"amber/__pycache__"
				"amber/_rdparm.cpython-39-x86_64-linux-gnu.so"
				"amber/_rdparm.cpython-38-x86_64-linux-gnu.so")))))))
	(build-system python-build-system)
	(arguments
	 '(;; Tests are broken in release
	   #:tests? #f
	   #:phases
	   (modify-phases %standard-phases
		 (add-after 'unpack 'versioneer
		   (lambda _
			 (invoke "versioneer" "install"))))))
	(native-inputs
	 (list python-versioneer))
	(propagated-inputs
	 (list python-numpy python-pandas))
	(home-page "https://parmed.github.io/ParmEd/html/index.html")
	(synopsis "Parameter/topology editor and molecular simulator")
	(description "ParmEd is a package designed to facilitate creating
and easily manipulating molecular systems that are fully described by
a common classical force field. Supported force fields include Amber,
CHARMM, AMOEBA, and several others that share a similar functional
form (e.g., GROMOS).")
	(license license:lgpl2.1)))

(define-public perl-math-vectorreal
  (package
    (name "perl-math-vectorreal")
    (version "1.02")
    (source
      (origin
        (method url-fetch)
        (uri (string-append
               "mirror://cpan/authors/id/A/AN/ANTHONY/Math-VectorReal-" version ".tar.gz"))
        (sha256
          (base32
            "12slrjwhxsvsn8f4byvbyx011j70r5mssrsm9l6yl2avhq09d5cc"))))
    (build-system perl-build-system)
    (home-page "https://metacpan.org/release/Math-VectorReal")
    (synopsis "Module to handle 3D Vector Mathematics")
    (description "The Math::VectorReal package defines a 3D mathematical
\"vector\", in a way that is compatible with the previous CPAN module
Math::MatrixReal. However it provides a more vector oriented set of
mathematical functions and overload operators, to the MatrixReal package.")
    (license license:gpl1+)))

(define-public perl-chemistry-mol
  (package
    (name "perl-chemistry-mol")
    (version "0.38")
    (source
      (origin
        (method url-fetch)
        (uri (string-append
               "mirror://cpan/authors/id/M/ME/MERKYS/Chemistry-Mol-" version ".tar.gz"))
        (sha256
          (base32
            "0wxwx3ivkbyvxaa5wx11zzk8qgfqh6dwc1iln650s4557qdrllld"))))
    (build-system perl-build-system)
    (propagated-inputs
      (list perl-io-string perl-math-vectorreal))
    (home-page
      "https://metacpan.org/release/Chemistry-Mol")
    (synopsis "Molecule object toolkit")
    (description "This package, along with Chemistry::Atom and Chemistry::Bond,
includes basic objects and methods to describe molecules.")
    (license license:perl-license)))

(define-public ambertools
  (package
	(name "ambertools")
	(version "21")
	(source (origin
			  (method url-fetch)
			  (uri "https://ambermd.org/downloads/AmberTools21jlmrcc.tar.bz2")
			  (sha256
			   (base32
				"1zda58k7zpb5agip11fr27wzrdya4lgjv8p896bqwnldb4qajpzm"))))
	(build-system cmake-build-system)
	(native-inputs
	 (list flex bison tcsh imake))
	(inputs
	 (list gfortran
		   netcdf
		   netcdf-fortran
		   boost
		   zlib
		   bzip2
		   tk
		   perl
		   perl-chemistry-mol
		   python
		   python-numpy
		   python-scipy
		   python-matplotlib
		   openblas
		   arpack-ng
		   fftw
		   libxext
		   libx11))
	(arguments
	 `(;; TODO
	   #:tests? #f
	   #:configure-flags
	   ,#~(list
		   (string-append "-DCMAKE_INSTALL_PREFIX=" #$output)
		   "-DCOMPILER=GNU"
		   "-DCMAKE_C_FLAGS=-fcommon"
		   "-DCMAKE_Fortran_FLAGS=-fallow-invalid-boz"
		   "-DCHECK_UPDATES=FALSE"
		   "-DSTATIC=TRUE"
		   "-DMPI=FALSE"
		   "-DCUDA=FALSE"
		   "-DOPENMP=TRUE"
		   "-DINSTALL_TESTS=FALSE"
		   "-DDOWNLOAD_MINICONDA=FALSE"
		   "-DTRUST_SYSTEM_LIBS=TRUE"
		   "-DFORCE_DISABLE_LIBS=plumed;mkl;pnetcdf"
		   "-DFORCE_EXTERNAL_LIBS=netcdf;netcdf-fortran;boost;arpack;fftw"
		   (string-append "-DNetCDF_INCLUDES=" #$(this-package-input "netcdf-fortran") "/include")
		   (string-append "-DNetCDF_LIBRARIES=" #$(this-package-input "netcdf-fortran") "/lib"))))
	(native-search-paths
	 (list (search-path-specification
			(variable "AMBERHOME")
			(files '("")))))

	(home-page "https://ambermd.org/AmberTools.php")
	(synopsis "A set of programs for biomolecular simulation and analysis (tools only)")
	(description "AmberTools consists of several independently developed
packages that work well by themselves, and with Amber20
itself. The suite can also be used to carry out complete
molecular dynamics simulations, with either explicit water or
generalized Born solvent models.")
	(license (list license:lgpl3+ license:gpl3))))

(define-public python-biopython-next
  (package
	(name "python-biopython-next")
	(version "1.79")
	(source
	 (origin
	   (method url-fetch)
	   (uri (pypi-uri "biopython" version))
	   (sha256
		(base32
		 "16smz3ncsarfgl74agrwzinzfqwjxknlpzsnpbbspf6kk6n7xc7d"))))
	(build-system python-build-system)
	(propagated-inputs
	 (list python-numpy))
	(home-page "https://biopython.org/")
	(synopsis
	 "Freely available tools for computational molecular biology.")
	(description
	 "Freely available tools for computational molecular biology.")
	(license (license:non-copyleft "https://github.com/biopython/biopython/blob/master/LICENSE.rst"))))

(define-public python-griddataformats
  (package
	(name "python-griddataformats")
	(version "0.5.0")
	(source
	 (origin
	   (method url-fetch)
	   (uri (pypi-uri "GridDataFormats" version))
	   (sha256
		(base32
		 "0scxqdq1hi4d07qmy1dij4r90bbj1yh8kkkn58djvqldf1hfs5zk"))))
	(build-system python-build-system)
	(native-inputs
	 (list python-pytest))
	(propagated-inputs
	 (list python-numpy python-scipy python-six))
	(home-page
	 "https://github.com/MDAnalysis/GridDataFormats")
	(synopsis
	 "Reading and writing of data on regular grids in Python")
	(description
	 "Reading and writing of data on regular grids in Python")
	(license license:lgpl3)))

(define-public python-mmtf-python
  (package
	(name "python-mmtf-python")
	(version "1.1.2")
	(source
	 (origin
	   (method url-fetch)
	   (uri (pypi-uri "mmtf-python" version))
	   (sha256
		(base32
		 "1px8nq1j14ap54rc5w0k6pnx7jvn68nxmcdm71ka3sn1sbyagjm5"))))
	(build-system python-build-system)
	(propagated-inputs
	 (list python-msgpack python-numpy))
	(native-inputs
	 (list python-check-manifest python-coverage))
	(home-page
	 "https://github.com/rcsb/mmtf-python.git")
	(synopsis
	 "A decoding libary for the PDB mmtf format")
	(description
	 "A decoding libary for the PDB mmtf format")
	(license license:asl2.0)))

(define-public python-funcsigs
  (package
	(name "python-funcsigs")
	(version "1.0.2")
	(source
	 (origin
	   (method url-fetch)
	   (uri (pypi-uri "funcsigs" version))
	   (sha256
		(base32
		 "0l4g5818ffyfmfs1a924811azhjj8ax9xd1cffr1mzd3ycn0zfx7"))))
	(build-system python-build-system)
	(native-inputs
	 (list python-unittest2))
	(home-page "http://funcsigs.readthedocs.org")
	(synopsis
	 "Python function signatures from PEP362 for Python 2.6, 2.7 and 3.2+")
	(description
	 "Python function signatures from PEP362 for Python 2.6, 2.7 and 3.2+")
	(license license:asl2.0)))

(define-public python-gsd
  (package
	(name "python-gsd")
	(version "2.4.2")
	(source
	 (origin
	   (method url-fetch)
	   (uri (pypi-uri "gsd" version))
	   (sha256
		(base32
		 "0laqh1nir576mi5xwda5hxnw302fv30ssbd20bmbyb7blcnxcbps"))))
	(build-system python-build-system)
	(native-inputs
	 (list python-pytest))
	(propagated-inputs
	 (list python-numpy python-cython))
	(home-page "https://gsd.readthedocs.io")
	(synopsis "General simulation data file format.")
	(description
	 "General simulation data file format.")
	(license (license:non-copyleft "https://gsd.readthedocs.io/en/stable/license.html"))))

(define-public python-mdanalysis
  (package
	(name "python-mdanalysis")
	(version "2.3.0")
	(source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://github.com/MDAnalysis/mdanalysis")
			 (commit (string-append "release-" version))))
	   (sha256
		(base32
		 "08jwqay8658n0qw798s90f68kp744xwbzwz27kqmqz64c0yvpk4x"))))
	(build-system python-build-system)
	(arguments
	 '(#:tests? #f
	   #:phases
	   (modify-phases %standard-phases
		 (add-before 'build 'change-directory
		   (lambda _
			 (chdir "package") #t)))))
	(native-inputs
	 (list python-mock python-pytest python-cython))
	(propagated-inputs
	 (list python-numpy
		   python-networkx
		   python-joblib
		   python-fasteners
		   python-scipy
		   python-matplotlib
		   python-tqdm
		   python-packaging
		   python-threadpoolctl
		   python-funcsigs
		   python-biopython-next
		   python-griddataformats
		   python-mmtf-python
		   python-gsd))
	(home-page
	 "http://docs.enthought.com/mayavi/mayavi/")
	(synopsis
	 "3D scientific data visualization library and application")
	(description
	 "3D scientific data visualization library and application")
	(license license:gpl2+)))

(define-public ovito
  (package
   (name "ovito")
   (version "3.7.12")
   (source (origin
			(method git-fetch)
			(uri (git-reference
				  (url "https://gitlab.com/stuko/ovito")
				  (commit (string-append "v" version))))
			(sha256
			 (base32
			  "0j8df2b9g9hdcc4s5fawn1wy8q8zrgpf3m98cx4r3qxkgsq2s749"))))
   (inputs
	(list qtbase-5
		  boost
		  ffmpeg
		  netcdf
		  hdf5
		  python
		  openssh
		  zlib
		  qtsvg-5))
   (build-system cmake-build-system)
   (arguments
	`(#:configure-flags
	  (let* ((out (assoc-ref %outputs "out"))
			 (runpath
			  (string-join (list (string-append out "/lib/ovito")
								 (string-append out "/lib/ovito/plugins"))
						   ";")))
		(list
		 (string-append "-DCMAKE_INSTALL_RPATH=" runpath)
		 "-DCMAKE_BUILD_TYPE=Release"
		 "-DOVITO_BUILD_DOCUMENTATION=OFF"))
	  #:phases
	  (modify-phases %standard-phases
					 (add-after 'install 'wrap-executable
								(lambda* (#:key inputs outputs #:allow-other-keys)
								  (let ((out (assoc-ref outputs "out"))
										(qt '("qtbase" "qtsvg")))
									(wrap-program (string-append out "/bin/ovito")
												  `("QT_PLUGIN_PATH" ":" prefix
													,(map (lambda (label)
															(string-append (assoc-ref inputs label)
																		   "/lib/qt5/plugins/"))
														  qt)))
									#t))))))
   (home-page "https://www.ovito.org/")
   (synopsis "A scientific data visualization and analysis software for atomistic simulation models")
   (description "OVITO is a scientific visualization and analysis software for
atomistic and particle simulation data. It helps scientists
gain better insights into materials phenomena and physical
processes.")
   (license (list license:gpl3))))

(define-public packmol
  (package
   (name "packmol")
   (version "20.11.1")
   (source (origin
			(method git-fetch)
			(uri (git-reference
				  (url "https://github.com/m3g/packmol")
				  (commit (string-append "v" version))))
			(file-name (git-file-name name version))
			(sha256
			 (base32
			  "0v9z1qjwznkm7rpy3ajq8wsa4yd8nr8iz82w5bs2pg9jzhjm088q"))))
   (native-inputs
	(list gfortran))
   (build-system gnu-build-system)
   (arguments
	(list
	 #:tests? #f
	 #:phases
	 #~(modify-phases
		%standard-phases
		(add-after 'unpack 'set-compiler
				   (lambda _
					 (substitute* "configure"
								  (("compiler=.*") "compiler=gfortran\n"))
					 (substitute* "Makefile"
								  (("-march=native") ""))))
		(replace 'install
				 (lambda _
				   (install-file "packmol"
								 (string-append #$output "/bin")))))))
   (home-page "http://leandro.iqm.unicamp.br/m3g/packmol/home.shtml")
   (synopsis "Initial configurations for Molecular Dynamics
Simulations by packing optimization")
   (description "PACKMOL creates an initial point for molecular
dynamics simulations by packing molecules in defined regions of space.
The packing guarantees that short range repulsive interactions do not
disrupt the simulations.")
   (license (list license:expat))))

(define-public apbs
  (package
   (name "apbs")
   (version "3.0.0")
   (source (origin
			(method url-fetch)
			(uri (string-append "https://github.com/Electrostatics/apbs/releases/download/v"
								version "/APBS-" version "_Source-Code.tar.gz"))
			(sha256
			 (base32
			  "1nybwmsrklsgfdacjgrlaqiny9bgc0kr8j266b8wpk85l8kv5y1w"))))
   (build-system cmake-build-system)
   (native-inputs
	`(("fetk-apbs-src"
	   ,(origin
		 (method git-fetch)
		 (uri (git-reference
			   (url "https://github.com/Electrostatics/FETK")
			   (commit "83c6508045eb478420ded093ea5bf499f48ee63d")))
		 (file-name (git-file-name "fetk-apbs" "83c6508"))
		 (sha256
		  (base32 "0jd5nnwz1l9mxc29w37hjschrsyqf41w6yg9id6rvmbqiv27fcil"))))
	  ("python" ,python)
	  ("python-numpy" ,python-numpy)))
   (arguments
	`(#:configure-flags
	  (list
	   (string-append "-DCMAKE_INSTALL_PREFIX=" (assoc-ref %outputs "out"))
	   "-DCMAKE_INSTALL_INCLUDEDIR=include"
	   "-DWITH_OMP=TRUE"
	   "-DCMAKE_EXE_LINKER_FLAGS=-Wl,--allow-multiple-definition"
	   "-DCMAKE_BUILD_TYPE=RelWithDebInfo")
	  #:phases
	  (modify-phases %standard-phases
					 (add-after 'unpack 'unpack-submodule-sources
								(lambda* (#:key inputs #:allow-other-keys)
								  (copy-recursively (assoc-ref inputs "fetk-apbs-src") "apbs/externals/fetk")
								  #t)))))
   (home-page "https://www.poissonboltzmann.org/")
   (synopsis "Software for biomolecular electrostatics and solvation")
   (description "APBS (Adaptive Poisson-Boltzmann Solver) solves the equations
of continuum electrostatics for large biomolecular
assemblages. This software was designed “from the ground up”
using modern design principles to ensure its ability to
interface with other computational packages and evolve as
methods and applications change over time.")
   (license (list license:bsd-3 license:lgpl2.1+))))
