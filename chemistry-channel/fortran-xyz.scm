;;; Copyright © 2022 David Elsing <david.elsing@kit.edu>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with This program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (chemistry-channel fortran-xyz)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix git)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages gcc))

(define-public fortran-json-fortran
  (package
	(name "fortran-json-fortran")
	(version "8.3.0")
	(source (origin
			  (method git-fetch)
			  (uri (git-reference
					(url "https://github.com/jacobwilliams/json-fortran")
					(commit version)))
			  (file-name (git-file-name name version))
			  (sha256
			   (base32
				"17imxd6ksmb04m38hj1qq9swp6yyqqyhsrqbwc6p2rw46mpvv9gp"))))
	(build-system cmake-build-system)
	(arguments
	 (list
	  #:configure-flags ''("-DUSE_GNU_INSTALL_CONVENTION=ON")
	  #:phases
	  #~(modify-phases %standard-phases
		  (replace 'check
			(lambda* (#:key (parallel-build? #t) #:allow-other-keys)
			  (invoke "make" "check"
					  "-j" (if parallel-build?
							   (number->string (parallel-job-count))
							   "1")))))))
	(native-inputs (list gfortran))
	(home-page "https://jacobwilliams.github.io/json-fortran/")
	(synopsis "Fortran library for reading and writing JSON files")
	(description "Thread-safe, object-oriented API for reading and
writing JSON files, written in modern Fortran.")
	(license license:bsd-3)))
