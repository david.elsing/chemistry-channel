(define-module (chemistry-channel compression)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages attr)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages linux)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu))

(define-public squashfuse
  (package
   (name "squashfuse")
   (version "0.1.105")
   (source (origin
			(method git-fetch)
			(uri (git-reference
				  (url "https://github.com/vasi/squashfuse")
				  (commit version)))
			(file-name (git-file-name name version))
			(sha256
			 (base32
			  "03aw8pw8694jyrzpnbry05rk9718sqw66kiyq878bbb679gl7224"))))
   (native-inputs (list
				   attr
				   autoconf
				   automake
				   fuse-3
				   libtool
				   lz4
				   lzo
				   pkg-config
				   xz
				   zlib
				   `(,zstd "lib")))
   (build-system gnu-build-system)
   (home-page "https://github.com/vasi/squashfuse")
   (synopsis "FUSE filesystem to mount squashfs archives")
   (description "")
   (license license:bsd-2)))
