;;; Copyright © 2022 David Elsing <david.elsing@kit.edu>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with This program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (chemistry-channel maths)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages python-xyz)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system python))

(define-public libtaylor
  (let ((commit "88709f03efda5b81ff460ccef67d4fd0e7d050cc")
        (revision "1"))
    (package
      (name "libtaylor")
      (version (git-version "0" "0" commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/uekstrom/libtaylor")
                      (commit commit)))
                (file-name (git-file-name "libtaylor" commit))
                (sha256
                 (base32
                  "17gp97vqlpmigf1rf1f5s8lavcswfvpyvqnxxjpdrz5dw9f51y6f"))))
      (build-system cmake-build-system)
      (arguments
       '(#:phases
         (modify-phases %standard-phases
           (delete 'check)
           (add-after 'install 'check
               (lambda args
                 (apply (assoc-ref %standard-phases 'check) args))))))
      (home-page "https://github.com/uekstrom/libtaylor")
      (synopsis "C++ library for automatic differentiation")
      (description "This is a header-only C++ library for calculating
analytical derivatives and taylor expansions of composite functions.")
      (license license:expat))))

(define-public python-correrr
  (package
	(name "python-correrr")
	(version "0.1")
	(source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://codeberg.org/dtelsing/correrr")
			 (commit version)))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "00gbvj5q1ca5yba3vip9rmdp7ida0db7zzj7fmflyh0fp1ab3v9q"))))
	(build-system python-build-system)
	(arguments
	 '(;; No tests
	   #:tests? #f))
	(propagated-inputs
	 (list python-numpy python-h5py))
	(home-page "")
	(synopsis "Estimate covariance matrix of the mean from correlated time series")
	(description "Estimate covariance matrix of the mean from correlated time series")
	(license license:gpl3+)))

(define-public bliss
  (package
    (name "bliss")
    (version "0.73")
    (source (origin
              (method url-fetch)
              (uri (string-append "http://www.tcs.hut.fi/Software/bliss/bliss-"
                                  version ".zip"))
              (sha256
               (base32
                "110ggzyn4fpsq3haisv7pzkgfs5x1m7802r4n5cas30l0hlg6yzm"))))
    (outputs (list "out" "doc"))
    (native-inputs (list doxygen unzip))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'fix-string-macro
           (lambda _
             (substitute* "bliss.cc"
               (("\"__DATE__\"") "\" __DATE__ \""))
             #t))
	 (add-after 'unpack 'disable-gmp
	   (lambda _
	     (substitute* "bignum.hh"
	       (("defined\\(BLISS_USE_GMP\\)") "0"))
	     #t))
         (add-after 'disable-gmp 'fix-includes
           (lambda _
             (substitute* (find-files "." "\\.(h|hh|cc)$")
               (("#include \"(.*)\"" all path)
                (string-append "#include <bliss/" path ">")))))
         (add-after 'fix-includes 'move-headers
           (lambda _
             (mkdir-p "include/bliss")
             (for-each
              (lambda (file)
                (rename-file file
                             (string-append "include/bliss/" (basename file))))
              (find-files "." "\\.(h|hh)$"))
             #t))
         (delete 'configure)
         (replace 'build
           (lambda _
             (let ((source-files
                    '("defs" "graph" "partition" "orbit" "uintseqhash" "heap"
                      "timer" "utils")))
               (for-each
                (lambda (file)
                  (display (string-append "Compiling " file "\n"))
                  (invoke "g++" "-Iinclude" "-fPIC" "-c" "-o"
                          (string-append file ".o")
                          (string-append file ".cc")))
                source-files)
               (apply invoke "ar" "cr" "bliss.a"
                      (map (lambda (file) (string-append file ".o"))
                           source-files))
               (display (string-append "Linking shared library\n"))
               (invoke "g++" "-Iinclude" "-fPIC" "-shared"
                       "-o" "libbliss.so"
                       "bliss_C.cc"
                       "-Wl,--whole-archive" "bliss.a" "-Wl,--no-whole-archive")
               (display (string-append "Building bliss\n"))
               (invoke "g++" "-Iinclude" "-o" "bliss" "bliss.cc"
                       "bliss.a"))
             #t))
         (add-after 'build 'build-doc
           (lambda _
             (invoke "doxygen")
             #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (install-file "libbliss.so" (string-append out "/lib"))
               (install-file "bliss" (string-append out "/bin"))
               (copy-recursively "include/bliss"
                                 (string-append out "/include/bliss")))
             #t))
         (add-after 'install 'install-doc
           (lambda* (#:key outputs #:allow-other-keys)
             (copy-recursively
              "html"
              (string-append (assoc-ref outputs "doc") "/share/doc/"
                             ,name "-" ,version "/html"))
             #t)))))
    (home-page "http://www.tcs.hut.fi/Software/bliss/index.shtml")
    (synopsis "Tool for computing automorphism groups and canonical labelings of graphs")
    (description "@code{bliss} is an open source tool for computing
automorphism groups and canonical forms of graphs.  It has both a command line
user interface as well as C++ and C programming language APIs.")
    (license license:lgpl3)))
